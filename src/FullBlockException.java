

/**
 * The FullBlockException class is meant to be a custom exception that is thrown
 * when a variable tries to add into the Block that already have the maximum
 * number of variables it can possibly hold
 */
public class FullBlockException extends Exception
{
    public FullBlockException(String msg)
    {
        super(msg+"\n");
    }
    
}